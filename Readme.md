# Seminar AngularJs 2016

## The slides

https://docs.google.com/presentation/d/1zPalTMg-D8QS-NW1qWLs7OuGJKuxederfHuXx332wOk

## Code usage

**Install the dependencies**

- `npm i && bower i`

**Start the application**

- `gulp`

**Open the application in the browser**

- `http://localhost:8500/`